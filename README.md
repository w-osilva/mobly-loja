**Requisitos técnicos:**
Elaborar um carrinho de compras simples (lista de categorias / lista de produtos / busca / detalhe / carrinho / criação do pedido) em PHP / Mysql  em Yii ou ZendFramwork 1 (Pode ser utilizado tecnologias complementares como caches/ motores de pesquisa / etc );
#
**Requisitos funcionais:**
Os produtos devem estar em um banco de dados relacional nos quais devem ser exibidos na lista;
Os produtos devem possuir os seguintes atributos : nome, descriçao, imagem, preço, categoria(um produto pode estar em mais de uma categoria) , caracteristicas (devem ser pré-definidas e associadas ao produto) ;
Não é necessário criar um layout elaborado;
O carrinho deve ser mantido mesmo se o usuário navegar em outra pagina (nova busca / listagem / ou detalhe do produto );
O pedido deve ser salvo no banco de dados contemplando todos os itens do carrinho e os dados do usuario ( Pessoais e de entrega );
Não é necessário integração de nenhuma forma de pagamento, apenas gerar o registro do pedido no banco de dados;
#
**Entrega:**
O código do projeto deve ser colocado no github ou bitbucket do candidato e deve ser enviado apenas o link publico do projeto (Código php, Bibliotecas, Estrutura do banco de dados - com dados para teste );
#
#Configurações de Ambiente#

**Fazendo o clone do projeto**

```
#!script
[user@linux]$~ cd seu-diretorio-de-projetos
[user@linux]$~ git clone https://bitbucket.org/w-osilva/mobly-loja.git

```
#
**O ambiente do projeto **

- vagrant
- php5.4
- apache
- mysql
- zend 1.12.9

#
**Iniciando o vagrant**

```
#!script
[user@linux]$~ cd mobly-loja
[user@linux]$~ vagrant up

```
#
**Criando banco e inserindo primeiros registros**

Nesse ponto será necessário entrar na vm através do *vagrant ssh*:
```
#!script
[user@linux]$~ vagrant ssh

```

Agora será necessário acessar o diretorio do projeto na vm:
```
#!script
[user@vagrant]$~ cd /var/www

```

Enfim execute o comando *mysqldump* para criar o banco de dados a partir do arquivo .sql existente no projeto:
```
#!script
[user@vagrant]$~ mysqldump -u root -p'root' < mobly.sql

```
#
**Obs:**

- Não precisa configurar vhost do apache pois o vagrant irá criá-lo com o nome do diretorio do projeto
- será necessário adicionar no seu ambiente hospedeiro o host para o projeto 
```
#!script
[user@linux]$~ sudo vim /etc/hosts

```
adicione a linha:
```
#!script
33.33.33.100    mobly-loja.dev
```

#
**Testando o projeto:**

Acessar via navegador atraves da url 
[http://mobly-loja.dev/]()
