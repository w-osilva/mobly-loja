<?php

class CheckoutController extends Zend_Controller_Action
{
    private $auth = true;
    private $client = true;
    private $addresses = true;
    private $sessionMobly = null;

    public function init()
    {
        if (!$this->auth) {
            $this->redirect('/auth');
        }
        $this->sessionMobly = Zend_Registry::get('mobly');
        $this->client = new Application_Model_Clients();
        $this->addresses = new Application_Model_Addresses();

    }

    public function indexAction()
    {
        //get client from database
        //by default will be in session when authenticated
        $client = $this->client->retrieveOne(1);
        $clientArray = $client->toArray();
        $addresses = $this->addresses->retrieveAllByClient(1);
        $addressesArray = $addresses->toArray();

        //set client data in session

        $this->redirect('/sales-order/');
    }

}











