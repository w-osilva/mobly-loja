<?php

class ProductsController extends Zend_Controller_Action
{
    private $model = null;
    
    public function init()
    {
        $this->model = new Application_Model_Products();
    }

    public function indexAction()
    { 
        $products = $this->model->retrieveAll();
        $this->view->assign('products', $products->toArray());
    }

    public function detailsAction()
    {
        try {
            $id = $this->_request->getParam('idProduct');
            $product = $this->model->retrieveOne($id);
            $this->view->assign('product', $product->toArray());
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }


}



