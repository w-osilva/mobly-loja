<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $sessionMobly = Zend_Registry::get('mobly');
        if (!isset($sessionMobly->init)) {
            $data = new Zend_Date();   
            $sessionMobly->init = sha1($data->get(Zend_Date::DATETIME_FULL));
        }
    }

    public function indexAction()
    {        
        $this->redirect('/products/');
    }


}

