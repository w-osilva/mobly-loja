<?php

class CartController extends Zend_Controller_Action
{
    private $product = null;

    private $sessionMobly = null;

    public function init()
    {
        $this->sessionMobly = Zend_Registry::get('mobly');
        if (!isset($this->sessionMobly->cart)) {
            $this->sessionMobly = array();
        }
        $this->product = new Application_Model_Products();
    }

    public function indexAction()
    {
//        if (!empty($this->sessionMobly->cart)) {
//            $this->redirect('/cart/empty');
//        }

        $this->view->assign('cart', $this->sessionMobly->cart[$this->sessionMobly->init]);
    }

    public function addAction()
    {
     	$this->getHelper('layout')->disableLayout();
     	$this->getHelper('viewRenderer')->setNoRender(true);
        
        $id = $this->_request->getParam('idProduct');
        $product = $this->product->retrieveOne($id);
        $productArray = $product->toArray();
        
        $this->sessionMobly->cart[$this->sessionMobly->init][$productArray['id']] = $product->toArray();
        $this->sessionMobly->cart[$this->sessionMobly->init][$productArray['id']]['quantity'] += 1;
        
        $totalPrice = 0;
        foreach ($this->sessionMobly->cart[$this->sessionMobly->init] as $products) {
            $totalPrice += ($products['price'] * $products['quantity']);
        }
        
        $this->sessionMobly->cart[$this->sessionMobly->init]['total_cart'] = $totalPrice;
        $this->sessionMobly->cart[$this->sessionMobly->init]['key_cart'] = $this->sessionMobly->init;
        $this->redirect('/cart/');
    }

    public function editAction()
    {
        // action body
    }

    public function deleteAction()
    {

    }

    public function retrieveAction()
    {
        // action body
    }

    public function emptyAction()
    {
        $this->view->assign('cart/empty');
    }


}











