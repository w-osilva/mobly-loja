<?php

class Application_Model_Addresses
{
    private $mapper;
    private $filters = array();

    public function __construct()
    {
        $this->mapper = new Application_Model_DbTable_Addresses();

        $this->filters = array(
            'id' => array(new Zend_Filter_Int()),
            'street' => array(new Zend_Filter_StringToUpper()),
            'complement' => array(new Zend_Filter_StringToUpper()),
            'number' => array(new Zend_Filter_Int()),
            'neighborhood' => array(new Zend_Filter_StringToUpper()),
            'neighborhood' => array(new Zend_Filter_StringToUpper()),
            'city' => array(new Zend_Filter_StringToUpper()),
            'state' => array(new Zend_Filter_StringToUpper()),
            'client_id' => array(new Zend_Filter_Int()),
            'type_id' => array(new Zend_Filter_Int()),
        );
    }

    public function retrieveAll()
    {
        return $this->mapper->fetchAll();
    }

    public function retrieveOne($id)
    {
        $id = (int) $id;
        $row = $this->mapper->find($id)->current();
        if (!$row) {
            throw new Exception('Could not find address ' . $id);
        }
        return $row;
    }

    public function retrieveAllByClient($id)
    {
        $id = (int) $id;

        $rows = $this->mapper->fetchAll(
            $this->mapper
                ->select()
                ->where('client_id = ?', $id)
        );

        if (!$rows) {
            throw new Exception('Could not find addresses of client ' . $id);
        }
        return $rows;
    }

}

