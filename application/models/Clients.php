<?php

class Application_Model_Clients
{
    private $mapper;
    private $filters = array();

    public function __construct()
    {
        $this->mapper = new Application_Model_DbTable_Clients();

        $this->filters = array(
            'id' => array(new Zend_Filter_Int()),
            'name' => array(new Zend_Filter_StringToUpper())
        );
    }

    public function retrieveAll()
    {
        return $this->mapper->fetchAll();
    }

    public function retrieveOne($id)
    {
        $id = (int) $id;
        $row = $this->mapper->find($id)->current();
        if (!$row) {
            throw new Exception('Could not find client ' . $id);
        }
        return $row;
    }
}

