<?php

class Application_Model_DbTable_ProductsHasCategories extends Zend_Db_Table_Abstract
{

    protected $_name = 'products_has_categories';
    protected $_referenceMap = array(
        'Products' => array(
            'columns' => 'product_id',
            'refTableClass' => 'Application_Model_DbTable_Products',
            'refColumns' => 'id'
        ),
        'Categories' => array(
            'columns' => 'category_id',
            'refTableClass' => 'Application_Model_DbTable_Categories',
            'refColumns' => 'id'
        )
    );

}

