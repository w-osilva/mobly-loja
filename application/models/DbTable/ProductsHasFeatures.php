<?php

class Application_Model_DbTable_ProductsHasFeatures extends Zend_Db_Table_Abstract
{

    protected $_name = 'products_has_Features';
    protected $_referenceMap = array(
        'Products' => array(
            'columns' => 'product_id',
            'refTableClass' => 'Application_Model_DbTable_Products',
            'refColumns' => 'id'
        ),
        'Features' => array(
            'columns' => 'feature_id',
            'refTableClass' => 'Application_Model_DbTable_Features',
            'refColumns' => 'id'
        )
    );

}

