<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public function __call($method, $arguments = null)
    {
            //three first caracters of call method 
            $methodType  = substr($method, 0, 3);
            $propertyName = strtolower(substr($method, 3, 1)) . substr($method, 4);
            if($methodType === 'set') 
            {
                    $this->$propertyName = $arguments[0];
                    return $this;
            }
            // if get, return value the properties
            elseif($methodType === 'get') 
            {
                    return $this->$propertyName;
            }
            //return exception not exists method
            else 
            {
                    throw new InvalidArgumentException(utf8_decode('O método ' . __CLASS__  . '::'. $method . '() não existe!'));
            }
    }
    
    protected function _initConfig()
    {
        Zend_Registry::set('config', new Zend_Config($this->getApplication()->getOptions(), true));
    }

    protected function _initSession()
    {
        Zend_Session::start();
        Zend_Registry::set('form', new Zend_Session_Namespace('form'));
        Zend_Registry::set('mobly', new Zend_Session_Namespace('mobly'));
    }
    
    protected function _initDatabase()
    {
        $this->bootstrap('db');
        $db = $this->getPluginResource('db')->getDbAdapter();
        Zend_Db_Table::setDefaultAdapter($db);
        Zend_Registry::set('database', $db);
    }
    
    protected function _initZFDebug()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('ZFDebug');

        $options = array(
            'plugins' => array('Variables',
                'File' => array('base_path' => APPLICATION_PATH),
                'Memory',
                'Time',
                'Registry',
                'Exception',
                'Cache' => array('backend' => APPLICATION_PATH . '/cache'))
        );

        # Instantiate the database adapter and setup the plugin.
        # Alternatively just add the plugin like above and rely on the autodiscovery feature.
        if ($this->hasPluginResource('db'))
        {
            $this->bootstrap('db');
            $db = $this->getPluginResource('db')->getDbAdapter();
            $options['plugins']['Database']['adapter'] = $db;
        }

        # Setup the cache plugin
        if ($this->hasPluginResource('cache'))
        {
            $this->bootstrap('cache');
            $cache = $this - getPluginResource('cache')->getDbAdapter();
            $options['plugins']['Cache']['backend'] = $cache->getBackend();
        }

        $debug = new ZFDebug_Controller_Plugin_Debug($options);

        $this->bootstrap('frontController');
        $frontController = $this->getResource('frontController');
        $frontController->registerPlugin($debug);
    }
    
    protected function _initCache()
    {
        $cache = Zend_Cache::factory(
                        'Core', 'File', array(
                        'caching' => true,
                        'cache_id_prefix' => null,
                        'lifetime' => 100,
                        'logging' => false,
                        'write_control' => true,
                        'automatic_serialization' => true,
                        'automatic_cleaning_factor' => 10,
                        'ignore_user_abort' => false),
                        array('cache_dir' => APPLICATION_PATH . '/data/cache'));
        Zend_Registry::set('cache', $cache);

        Zend_Locale::setCache($cache);
        Zend_Translate::setCache($cache);

        /**
         * Grava em cache os meta data do Zend_Db
         */
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
    }
    
    protected function _initLog()
    {
        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/log/debug.log');
        $log = new Zend_Log($writer);
        Zend_Registry::set('log', $log);
    }
    
    public function _initRouter()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        
        $route = new Zend_Controller_Router_Route(
            'products/details/:idProduct',
            array(
                'controller' => 'products', 
                'action' => 'details'
            ),						
            array('idProduct' => '\d+')
        );
        $router->addRoute('details', $route);
        
        $route = new Zend_Controller_Router_Route(
            'cart/add/:idProduct',
            array(
                'controller' => 'cart', 
                'action' => 'add'
            ),						
            array('idProduct' => '\d+')
        );
        $router->addRoute('add', $route);
//        
//        $route = new Zend_Controller_Router_Route(
//            'cart/show/:idProduct',
//            array(
//                'controller' => 'cart', 
//                'action' => 'add'
//            ),						
//            array('idProduct' => '\d+')
//        );
//        $router->addRoute('add', $route);
    }
}

